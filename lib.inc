section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:     
	mov rax, 60
	syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    	xor rax, rax
    	.loop:
      		cmp byte [rdi+rax], 0
      		je .end
      		inc rax
      		jmp .loop
    	.end:
      		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
    	pop rdi
    	mov  rdx, rax
    	mov  rsi, rdi
    	mov  rax, 1
    	mov  rdi, 1
    	syscall
    	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi		
	mov rdx, 1		
	mov rsi, rsp		
	mov rdi, 1		
	mov rax, 1		
	syscall
	pop rdi		
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA		
	call print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r10, 10
	mov rax, rdi
	push r12
	mov r12, rsp
	
	dec rsp
	mov byte [rsp], 0
	
	.loop:
		xor rdx, rdx
		div r10
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax
		jnz .loop
		
	mov rdi, rsp
	call print_string
	mov rsp, r12
	pop r12
	ret


	

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    	cmp rdi, 0
    	jge .out
    	push rdi
    	mov rdi, '-'
    	call print_char
    	pop rdi
    	neg rdi
    	.out:
    		call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	call string_length
	mov rdx, rax	
	push rdi
	mov rdi, rsi
	call string_length
	mov rcx, rax	
	mov rsi, rdi
	pop rdi
	cmp rdx, rcx
	jne .notequals
	xor rax, rax
	.loop:				
		mov al, byte [rdi]
		cmp al, byte [rsi]
		jne .notequals	
		inc rdi	
		inc rsi	
		cmp al, 0		
		jne .loop		
	.equals:				
		xor rax, rax
		inc rax
		ret
	
	.notequals:				
		xor rax, rax
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
	push rax
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret	

    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r12
	push r13
	push r14
	mov r14, rdi
	mov r12, rsi
	dec r12
	xor r13, r13

	.del_spaces:
		call read_char
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .del_spaces
		cmp rax, 0x9
		je .del_spaces
		cmp rax, 0xA
		je .del_spaces

	.loop:
		cmp r12, r13
		je .bad

		mov byte [r14+r13], al
		inc r13
		call read_char
		
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .end
		cmp rax, 0x9
		je .end
		cmp rax, 0xA
		je .end
		
		jmp .loop

	.bad:
		xor rax, rax
		pop r14
		pop r13
		pop r12
		ret

	.end:
		mov rax, r14
		mov rdx, r13
		mov byte[r14+r13], 0
		pop r14
		pop r13
		pop r12
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor r9, r9		
	mov r10, 10		
	xor r11, r11
	
	.loop:
		mov r11b, byte[rdi]
		cmp r11b, '0'
		jb .digit_end
		cmp r11b, '9'
		ja .digit_end
		mul r10
		sub r11b, '0'
		add rax, r11
		inc rdi
		inc r9
		jmp .loop
	
	.digit_end:
		mov rdx, r9
		ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, byte[rdi]
	cmp al, '-'
	
	jne .readpositive
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret
	
	.readpositive:
		jmp parse_uint



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	ja .error
	
	.loop:
		xor r9, r9
		mov r9b, byte[rdi]
		mov byte[rsi], r9b
		inc rdi
		inc rsi
		cmp r9b, 0
		je .end
		jmp .loop
		
	.end:
		ret
			
	.error:
		xor rax, rax
		ret	
